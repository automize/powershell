﻿#region functions

<#
.DESCRIPTION
   The Get-SNObject funtion will retriev an object from SericeNow

.EXAMPLE
   Get-SNObject -InstanceName dev12345 -TableName incident -Credential $Cred

.EXAMPLE
   Get-SNObject -InstanceName dev12345 -TableName incident -QueryParameters @{"state"=1} -Credential $Cred

.EXAMPLE
   Get-SNObject -InstanceName dev12345 -TableName incident -QueryParameters @{"short_description"="Incident Title";"state"=1} -Credential $Cred

.PARAMETER InstaceName
   name of the Service Now Instance to use

.PARAMETER TableName
   name of the table to use in Service Now

.PARAMETER SNFieldName
   name of the field to use in Service Now

.PARAMETER Credential
   PSCredential with permissions in Service Now

.OUTPUTS
   Outputs objects from Service Now in JSON format


.FUNCTIONALITY
   Retrieve objects from a given Service Now instance

#>
Function Get-SNObject {
[CmdletBinding(SupportsShouldProcess=$true)]
param
(
  [String]
  [Parameter(Mandatory=$true)]
  $InstanceName,

  [string]
  [Parameter(Mandatory=$true)]
  $TableName,

  [Hashtable]
  [Parameter(Mandatory=$false)]
  $QueryParameters,

  [Parameter(Mandatory=$true)][System.Management.Automation.PSCredential]$Credential
)

Begin
    {
    }
    Process
    {
        if ($pscmdlet.ShouldProcess("ServiceNow", "Get Object"))
        {
        
            # Build auth header
            $base64AuthInfo = [Convert]::ToBase64String([Text.Encoding]::ASCII.GetBytes(("{0}:{1}" -f $Credential.UserName, $Credential.GetNetworkCredential().Password)))

            # Set proper headers
            $headers = New-Object "System.Collections.Generic.Dictionary[[String],[String]]"
            $headers.Add('Authorization',('Basic {0}' -f $base64AuthInfo))
            $headers.Add('Accept','application/json')

            # Build Query string (if any query parameters specified)
            if($QueryParameters -ne $null -and $QueryParameters.Count -gt 0){

            #Build up the Query String
            $Query = "`?sysparm_query="
           
            $QueryParameters.GetEnumerator() | ForEach-Object {
            $Query += $PSItem.Key +"%3D" + $PSItem.Value + "%5E"
            return $Query
            }

            $Query = $Query.Substring(0, $Query.Length -3)

                # Specify endpoint uri
                $uri = "https://$InstanceName.service-now.com/api/now/table/$TableName$Query"
                Write-Verbose "URI is set to: $uri"
            }
            else{
               # Specify endpoint uri
                $uri = "https://$InstanceName.service-now.com/api/now/table/$TableName"
                Write-Verbose "URI is set to: $uri"
            }

            # Specify HTTP method
            $method = "get"
            Write-Verbose "HTTP method is: $method"


            #{request.body ? "$body = \"" :""}"}

            # Send HTTP request
            try{
                $response = Invoke-WebRequest -Headers $headers -Method $method -Uri $uri -UseBasicParsing -ErrorAction Stop

                add-type -assembly system.web.extensions


            $Obj = New-Object -TypeName System.Web.Script.Serialization.JavaScriptSerializer

            #Manipulate the JSON Max length
            $obj.MaxJsonLength = 99999999


            $JSON = $obj.DeserializeObject($response)


            if($JSON.result.count -eq '0'){
                Write-Output '0 results returned.'
            }
            else
            {
                $JSON.result
            }



            }
            catch{
            throw $PSItem.exception
            }

                    }
                }
                End
                {
                }

}#End of Function Get-SNObject

<#
.DESCRIPTION
   The New-SNObject funtion will create a new object in SericeNow

.EXAMPLE
   New-SNObject -InstanceName "dev12345" -TableName "cmdb_ci_computer" -Content @{'fqdn' = "test123.test.local"; 'name' = "test123"; 'install_status' = "1"} -Credential $Cred -Verbose

.PARAMETER InstaceName
   name of the Service Now Instance to use

.PARAMETER TableName
   name of the table to use in Service Now

.PARAMETER Content
   hashtable with values to set at Service Now

.PARAMETER Credential
   PSCredential with permissions in Service Now

.OUTPUTS
   Outputs object from Service Now in JSON format

.FUNCTIONALITY
   create a new object in a Service Now instance

#>
Function New-SNObject {

[CmdletBinding(SupportsShouldProcess=$true)]
Param(
   [Parameter(Mandatory=$true)][string]$InstanceName,
   [Parameter(Mandatory=$true)][string]$TableName,
   [Parameter(Mandatory=$true)][Hashtable]$Content,
   [Parameter(Mandatory=$true)][System.Management.Automation.PSCredential]$Credential
   )

Begin
    {
    }
    Process
    {
        if ($pscmdlet.ShouldProcess("ServiceNow", "New Object"))
        {
            # Build auth header
            $base64AuthInfo = [Convert]::ToBase64String([Text.Encoding]::ASCII.GetBytes(("{0}:{1}" -f $Credential.UserName, $Credential.GetNetworkCredential().Password)))


            # Set proper headers for new ServiceNow Object
            $headers = New-Object "System.Collections.Generic.Dictionary[[String],[String]]"
            $headers.Add('Authorization',('Basic {0}' -f $base64AuthInfo))
            $headers.Add('Accept','application/json')
            $headers.Add('Content-Type','application/json')


            # Specify endpoint uri
            $uri = "https://$InstanceName.service-now.com/api/now/table/$TableName"

            # Specify HTTP method
            $method = "post"

            # Specify request body
            $body = $Content | ConvertTo-Json

            try{
  
            # Send HTTP request
            $response = Invoke-WebRequest -Headers $headers -Method $method -Uri $uri -Body $body -UseBasicParsing -Verbose -ErrorAction Stop

            $JSON = $response | ConvertFrom-Json

            $JSON.result
            }
            catch{
            throw $PSItem.exception
            }

        
        }
                }
                End
                {
                } 

}#End of Function New-SNObject

<#
.DESCRIPTION
   The Update-SNObject funtion will update a specific object in SericeNow


.EXAMPLE
   Update-SNObject -InstanceName "dev12152" -TableName "cmdb_ci_computer" -Content @{'install_status' = "3"} -SysID "c4b2d2434f002200b816b4a18110c71b" -Credential $Cred -Verbose

.PARAMETER InstaceName
   name of the Service Now Instance to use

.PARAMETER TableName
   name of the table to use in Service Now

.PARAMETER Content
   hashtable with values to set at Service Now

.PARAMETER SysID
    SysID of the Service Now object about to be updated - can be retrived by using Get-SNObject CMDLet

.PARAMETER Credential
   PSCredential with permissions in Service Now

.OUTPUTS
   Outputs object from Service Now in JSON format


.FUNCTIONALITY
   update a specific object in a Service Now instance

#>
Function Update-SNObject {
[CmdletBinding(SupportsShouldProcess=$true)]
    Param(
   [Parameter(Mandatory=$true)][string]$InstanceName,
   [Parameter(Mandatory=$true)][string]$TableName,
   [Parameter(Mandatory=$true)][Hashtable]$Content,
   [Parameter(Mandatory=$true)][string]$SysID,
   [Parameter(Mandatory=$true)][System.Management.Automation.PSCredential]$Credential
   )

Begin
    {
    }
    Process
    {
        if ($pscmdlet.ShouldProcess("ServiceNow", "Update Object"))
        {
            
            # Build auth header
            $base64AuthInfo = [Convert]::ToBase64String([Text.Encoding]::ASCII.GetBytes(("{0}:{1}" -f $Credential.UserName, $Credential.GetNetworkCredential().Password)))

            # Set proper headers
            $headers = New-Object "System.Collections.Generic.Dictionary[[String],[String]]"
            $headers.Add('Authorization',('Basic {0}' -f $base64AuthInfo))
            $headers.Add('Accept','application/json')
            $headers.Add('Content-Type','application/json')


            # Specify endpoint uri
            $uri = "https://$InstanceName.service-now.com/api/now/table/$TableName/$SysID"


            # Specify HTTP method
            $method = "put"

            # Specify request body
            $body = $Content | ConvertTo-Json

            try{

            # Send HTTP request
            $response = Invoke-WebRequest -Headers $headers -Method $method -Uri $uri -Body $body -UseBasicParsing -Verbose -ErrorAction Stop

            $JSON = $response | ConvertFrom-Json

            $JSON.result

            }
            catch{
            throw $PSItem.exception
            }
        
        }
                }
                End
                {
                }

}#End of Function Update-SNObject

<#
.DESCRIPTION
   The Remove-SNObject funtion will remove a specific object in SericeNow

.EXAMPLE
   Remove-SNObject -InstanceName "dev12345" -TableName "cmdb_ci_computer" -SysID "c4b2d2434f002200b816b4a18110c71b" -Credential $Cred

.PARAMETER InstaceName
   name of the Service Now Instance to use

.PARAMETER TableName
   name of the table to use in Service Now

.PARAMETER SysID
    SysID of the Service Now object about to be removed - can be retrived by using Get-SNObject CMDLet

.PARAMETER Credential
   PSCredential with permissions in Service Now

.OUTPUTS
   Outputs object from Service Now in JSON format


.FUNCTIONALITY
   update a specific object in a Service Now instance

#>
Function Remove-SNObject {
[CmdletBinding(SupportsShouldProcess=$true)]
 Param(
   [Parameter(Mandatory=$true)][string]$InstanceName,
   [Parameter(Mandatory=$true)][string]$TableName,
   [Parameter(Mandatory=$true)][string]$SysID,
   [Parameter(Mandatory=$true)][System.Management.Automation.PSCredential]$Credential
   )

   Begin
    {
    }
    Process
    {
        if ($pscmdlet.ShouldProcess("ServiceNow", "Remove Object"))
        {
            
            # Build auth header
            $base64AuthInfo = [Convert]::ToBase64String([Text.Encoding]::ASCII.GetBytes(("{0}:{1}" -f $Credential.UserName, $Credential.GetNetworkCredential().Password)))

            # Set proper headers
            $headers = New-Object "System.Collections.Generic.Dictionary[[String],[String]]"
            $headers.Add('Authorization',('Basic {0}' -f $base64AuthInfo))
            $headers.Add('Accept','application/json')
            $headers.Add('Content-Type','application/json')


            # Specify endpoint uri
            $uri = "https://$InstanceName.service-now.com/api/now/table/$TableName/$SysID"


            # Specify HTTP method
            $method = "delete"

            try{
            # Send HTTP request
            $response = Invoke-WebRequest -Headers $headers -Method $method -Uri $uri -UseBasicParsing -Verbose -ErrorAction Stop

            # Print response
            return $response.RawContent

            }
            catch{
            throw $PSItem.exception
            }
        
        
        
        }
                }
                End
                {
                } 

}#End of Function Remove-SNObject

#endregion

#region export functions
Export-ModuleMember -Function Get-SNObject

Export-ModuleMember -Function New-SNObject

Export-ModuleMember -Function Update-SNObject

Export-ModuleMember -Function Remove-SNObject
#endregion


# SIG # Begin signature block
# MIIXwwYJKoZIhvcNAQcCoIIXtDCCF7ACAQExCzAJBgUrDgMCGgUAMGkGCisGAQQB
# gjcCAQSgWzBZMDQGCisGAQQBgjcCAR4wJgIDAQAABBAfzDtgWUsITrck0sYpfvNR
# AgEAAgEAAgEAAgEAAgEAMCEwCQYFKw4DAhoFAAQUZY8Up+W6oFbckvp+l6IdH+k8
# xmWgghL2MIID7jCCA1egAwIBAgIQfpPr+3zGTlnqS5p31Ab8OzANBgkqhkiG9w0B
# AQUFADCBizELMAkGA1UEBhMCWkExFTATBgNVBAgTDFdlc3Rlcm4gQ2FwZTEUMBIG
# A1UEBxMLRHVyYmFudmlsbGUxDzANBgNVBAoTBlRoYXd0ZTEdMBsGA1UECxMUVGhh
# d3RlIENlcnRpZmljYXRpb24xHzAdBgNVBAMTFlRoYXd0ZSBUaW1lc3RhbXBpbmcg
# Q0EwHhcNMTIxMjIxMDAwMDAwWhcNMjAxMjMwMjM1OTU5WjBeMQswCQYDVQQGEwJV
# UzEdMBsGA1UEChMUU3ltYW50ZWMgQ29ycG9yYXRpb24xMDAuBgNVBAMTJ1N5bWFu
# dGVjIFRpbWUgU3RhbXBpbmcgU2VydmljZXMgQ0EgLSBHMjCCASIwDQYJKoZIhvcN
# AQEBBQADggEPADCCAQoCggEBALGss0lUS5ccEgrYJXmRIlcqb9y4JsRDc2vCvy5Q
# WvsUwnaOQwElQ7Sh4kX06Ld7w3TMIte0lAAC903tv7S3RCRrzV9FO9FEzkMScxeC
# i2m0K8uZHqxyGyZNcR+xMd37UWECU6aq9UksBXhFpS+JzueZ5/6M4lc/PcaS3Er4
# ezPkeQr78HWIQZz/xQNRmarXbJ+TaYdlKYOFwmAUxMjJOxTawIHwHw103pIiq8r3
# +3R8J+b3Sht/p8OeLa6K6qbmqicWfWH3mHERvOJQoUvlXfrlDqcsn6plINPYlujI
# fKVOSET/GeJEB5IL12iEgF1qeGRFzWBGflTBE3zFefHJwXECAwEAAaOB+jCB9zAd
# BgNVHQ4EFgQUX5r1blzMzHSa1N197z/b7EyALt0wMgYIKwYBBQUHAQEEJjAkMCIG
# CCsGAQUFBzABhhZodHRwOi8vb2NzcC50aGF3dGUuY29tMBIGA1UdEwEB/wQIMAYB
# Af8CAQAwPwYDVR0fBDgwNjA0oDKgMIYuaHR0cDovL2NybC50aGF3dGUuY29tL1Ro
# YXd0ZVRpbWVzdGFtcGluZ0NBLmNybDATBgNVHSUEDDAKBggrBgEFBQcDCDAOBgNV
# HQ8BAf8EBAMCAQYwKAYDVR0RBCEwH6QdMBsxGTAXBgNVBAMTEFRpbWVTdGFtcC0y
# MDQ4LTEwDQYJKoZIhvcNAQEFBQADgYEAAwmbj3nvf1kwqu9otfrjCR27T4IGXTdf
# plKfFo3qHJIJRG71betYfDDo+WmNI3MLEm9Hqa45EfgqsZuwGsOO61mWAK3ODE2y
# 0DGmCFwqevzieh1XTKhlGOl5QGIllm7HxzdqgyEIjkHq3dlXPx13SYcqFgZepjhq
# IhKjURmDfrYwggSjMIIDi6ADAgECAhAOz/Q4yP6/NW4E2GqYGxpQMA0GCSqGSIb3
# DQEBBQUAMF4xCzAJBgNVBAYTAlVTMR0wGwYDVQQKExRTeW1hbnRlYyBDb3Jwb3Jh
# dGlvbjEwMC4GA1UEAxMnU3ltYW50ZWMgVGltZSBTdGFtcGluZyBTZXJ2aWNlcyBD
# QSAtIEcyMB4XDTEyMTAxODAwMDAwMFoXDTIwMTIyOTIzNTk1OVowYjELMAkGA1UE
# BhMCVVMxHTAbBgNVBAoTFFN5bWFudGVjIENvcnBvcmF0aW9uMTQwMgYDVQQDEytT
# eW1hbnRlYyBUaW1lIFN0YW1waW5nIFNlcnZpY2VzIFNpZ25lciAtIEc0MIIBIjAN
# BgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAomMLOUS4uyOnREm7Dv+h8GEKU5Ow
# mNutLA9KxW7/hjxTVQ8VzgQ/K/2plpbZvmF5C1vJTIZ25eBDSyKV7sIrQ8Gf2Gi0
# jkBP7oU4uRHFI/JkWPAVMm9OV6GuiKQC1yoezUvh3WPVF4kyW7BemVqonShQDhfu
# ltthO0VRHc8SVguSR/yrrvZmPUescHLnkudfzRC5xINklBm9JYDh6NIipdC6Anqh
# d5NbZcPuF3S8QYYq3AhMjJKMkS2ed0QfaNaodHfbDlsyi1aLM73ZY8hJnTrFxeoz
# C9Lxoxv0i77Zs1eLO94Ep3oisiSuLsdwxb5OgyYI+wu9qU+ZCOEQKHKqzQIDAQAB
# o4IBVzCCAVMwDAYDVR0TAQH/BAIwADAWBgNVHSUBAf8EDDAKBggrBgEFBQcDCDAO
# BgNVHQ8BAf8EBAMCB4AwcwYIKwYBBQUHAQEEZzBlMCoGCCsGAQUFBzABhh5odHRw
# Oi8vdHMtb2NzcC53cy5zeW1hbnRlYy5jb20wNwYIKwYBBQUHMAKGK2h0dHA6Ly90
# cy1haWEud3Muc3ltYW50ZWMuY29tL3Rzcy1jYS1nMi5jZXIwPAYDVR0fBDUwMzAx
# oC+gLYYraHR0cDovL3RzLWNybC53cy5zeW1hbnRlYy5jb20vdHNzLWNhLWcyLmNy
# bDAoBgNVHREEITAfpB0wGzEZMBcGA1UEAxMQVGltZVN0YW1wLTIwNDgtMjAdBgNV
# HQ4EFgQURsZpow5KFB7VTNpSYxc/Xja8DeYwHwYDVR0jBBgwFoAUX5r1blzMzHSa
# 1N197z/b7EyALt0wDQYJKoZIhvcNAQEFBQADggEBAHg7tJEqAEzwj2IwN3ijhCcH
# bxiy3iXcoNSUA6qGTiWfmkADHN3O43nLIWgG2rYytG2/9CwmYzPkSWRtDebDZw73
# BaQ1bHyJFsbpst+y6d0gxnEPzZV03LZc3r03H0N45ni1zSgEIKOq8UvEiCmRDoDR
# EfzdXHZuT14ORUZBbg2w6jiasTraCXEQ/Bx5tIB7rGn0/Zy2DBYr8X9bCT2bW+IW
# yhOBbQAuOA2oKY8s4bL0WqkBrxWcLC9JG9siu8P+eJRRw4axgohd8D20UaF5Mysu
# e7ncIAkTcetqGVvP6KUwVyyJST+5z3/Jvz4iaGNTmr1pdKzFHTx/kuDDvBzYBHUw
# ggUlMIIEDaADAgECAhAG+v2wvXnjGcvHbHMjoqQyMA0GCSqGSIb3DQEBCwUAMHIx
# CzAJBgNVBAYTAlVTMRUwEwYDVQQKEwxEaWdpQ2VydCBJbmMxGTAXBgNVBAsTEHd3
# dy5kaWdpY2VydC5jb20xMTAvBgNVBAMTKERpZ2lDZXJ0IFNIQTIgQXNzdXJlZCBJ
# RCBDb2RlIFNpZ25pbmcgQ0EwHhcNMTYxMDE0MDAwMDAwWhcNMTkxMDIzMTIwMDAw
# WjBiMQswCQYDVQQGEwJESzETMBEGA1UECBMKU3lkZGFubWFyazEQMA4GA1UEBxMH
# S29sZGluZzEVMBMGA1UEChMMQXV0b21pemUgUC9TMRUwEwYDVQQDEwxBdXRvbWl6
# ZSBQL1MwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDD2i1LjPMTWQ/Z
# IUu9hVd4zXoU0k4xoRI1hdS4T2uvNdQPIGksD0SIK86+cBqKLVe8V+Do4mkKmmc3
# OskJnetN0ebst/8/ssJBSTc9zrEjmMdwspRog0Wjnb37bJWo0/Oown86BPafFCJT
# oFoTBq4rnERtHAX8W7eNky8Lv7VMzcyM7poaeh7LSRhoetd1j6WpOWlAnbzu+tis
# kixhl+uViQRBMl7nAzHxAvoq+Qx9fF4WhRXP1IhL0I+xiEdFiUQBaEOGBX7H/yXc
# qmK7yWux+mpCd1L+t9LjqwiG2YjamlzaRaq9xd8C/HNUX/c/mnQltsWE8GuMoTpX
# oL+eEq8fAgMBAAGjggHFMIIBwTAfBgNVHSMEGDAWgBRaxLl7KgqjpepxA8Bg+S32
# ZXUOWDAdBgNVHQ4EFgQUtkZqp/lfcNEd55nXFliTnGvgwoMwDgYDVR0PAQH/BAQD
# AgeAMBMGA1UdJQQMMAoGCCsGAQUFBwMDMHcGA1UdHwRwMG4wNaAzoDGGL2h0dHA6
# Ly9jcmwzLmRpZ2ljZXJ0LmNvbS9zaGEyLWFzc3VyZWQtY3MtZzEuY3JsMDWgM6Ax
# hi9odHRwOi8vY3JsNC5kaWdpY2VydC5jb20vc2hhMi1hc3N1cmVkLWNzLWcxLmNy
# bDBMBgNVHSAERTBDMDcGCWCGSAGG/WwDATAqMCgGCCsGAQUFBwIBFhxodHRwczov
# L3d3dy5kaWdpY2VydC5jb20vQ1BTMAgGBmeBDAEEATCBhAYIKwYBBQUHAQEEeDB2
# MCQGCCsGAQUFBzABhhhodHRwOi8vb2NzcC5kaWdpY2VydC5jb20wTgYIKwYBBQUH
# MAKGQmh0dHA6Ly9jYWNlcnRzLmRpZ2ljZXJ0LmNvbS9EaWdpQ2VydFNIQTJBc3N1
# cmVkSURDb2RlU2lnbmluZ0NBLmNydDAMBgNVHRMBAf8EAjAAMA0GCSqGSIb3DQEB
# CwUAA4IBAQA0zGCo40Bv+7KNre6TTDWf/2k0Tzauzwy3oesHg3pd7ZgQcPTU5er3
# vLXCKQwb8bmsPw3XpL0u1CkjdiqaO6S4qv3pGbyg9qnDXeFPJhg+i5VaTGb+PMSa
# +nquQtdHhxfj5VEdt6nJlG/cBPMf/cH3O7/AzngCY1KGClKhtgLphHVF15oMj2Rt
# Bu/jHSqWQAZOIXg7lvNfcRDI6DHwtMXVa4pVDJJM26SsLfpdoJnVOAbWUbkzf8BX
# QDDp/pG/M9mrADKmPb+dy5EnX0SWwyZd3t/cIw7+AkpXbKGv3TJLSKQMdvW+bqZ0
# /VCcF796baLjQYXi28I4pkAabr5gt2u4MIIFMDCCBBigAwIBAgIQBAkYG1/Vu2Z1
# U0O1b5VQCDANBgkqhkiG9w0BAQsFADBlMQswCQYDVQQGEwJVUzEVMBMGA1UEChMM
# RGlnaUNlcnQgSW5jMRkwFwYDVQQLExB3d3cuZGlnaWNlcnQuY29tMSQwIgYDVQQD
# ExtEaWdpQ2VydCBBc3N1cmVkIElEIFJvb3QgQ0EwHhcNMTMxMDIyMTIwMDAwWhcN
# MjgxMDIyMTIwMDAwWjByMQswCQYDVQQGEwJVUzEVMBMGA1UEChMMRGlnaUNlcnQg
# SW5jMRkwFwYDVQQLExB3d3cuZGlnaWNlcnQuY29tMTEwLwYDVQQDEyhEaWdpQ2Vy
# dCBTSEEyIEFzc3VyZWQgSUQgQ29kZSBTaWduaW5nIENBMIIBIjANBgkqhkiG9w0B
# AQEFAAOCAQ8AMIIBCgKCAQEA+NOzHH8OEa9ndwfTCzFJGc/Q+0WZsTrbRPV/5aid
# 2zLXcep2nQUut4/6kkPApfmJ1DcZ17aq8JyGpdglrA55KDp+6dFn08b7KSfH03sj
# lOSRI5aQd4L5oYQjZhJUM1B0sSgmuyRpwsJS8hRniolF1C2ho+mILCCVrhxKhwjf
# DPXiTWAYvqrEsq5wMWYzcT6scKKrzn/pfMuSoeU7MRzP6vIK5Fe7SrXpdOYr/mzL
# fnQ5Ng2Q7+S1TqSp6moKq4TzrGdOtcT3jNEgJSPrCGQ+UpbB8g8S9MWOD8Gi6CxR
# 93O8vYWxYoNzQYIH5DiLanMg0A9kczyen6Yzqf0Z3yWT0QIDAQABo4IBzTCCAckw
# EgYDVR0TAQH/BAgwBgEB/wIBADAOBgNVHQ8BAf8EBAMCAYYwEwYDVR0lBAwwCgYI
# KwYBBQUHAwMweQYIKwYBBQUHAQEEbTBrMCQGCCsGAQUFBzABhhhodHRwOi8vb2Nz
# cC5kaWdpY2VydC5jb20wQwYIKwYBBQUHMAKGN2h0dHA6Ly9jYWNlcnRzLmRpZ2lj
# ZXJ0LmNvbS9EaWdpQ2VydEFzc3VyZWRJRFJvb3RDQS5jcnQwgYEGA1UdHwR6MHgw
# OqA4oDaGNGh0dHA6Ly9jcmw0LmRpZ2ljZXJ0LmNvbS9EaWdpQ2VydEFzc3VyZWRJ
# RFJvb3RDQS5jcmwwOqA4oDaGNGh0dHA6Ly9jcmwzLmRpZ2ljZXJ0LmNvbS9EaWdp
# Q2VydEFzc3VyZWRJRFJvb3RDQS5jcmwwTwYDVR0gBEgwRjA4BgpghkgBhv1sAAIE
# MCowKAYIKwYBBQUHAgEWHGh0dHBzOi8vd3d3LmRpZ2ljZXJ0LmNvbS9DUFMwCgYI
# YIZIAYb9bAMwHQYDVR0OBBYEFFrEuXsqCqOl6nEDwGD5LfZldQ5YMB8GA1UdIwQY
# MBaAFEXroq/0ksuCMS1Ri6enIZ3zbcgPMA0GCSqGSIb3DQEBCwUAA4IBAQA+7A1a
# JLPzItEVyCx8JSl2qB1dHC06GsTvMGHXfgtg/cM9D8Svi/3vKt8gVTew4fbRknUP
# UbRupY5a4l4kgU4QpO4/cY5jDhNLrddfRHnzNhQGivecRk5c/5CxGwcOkRX7uq+1
# UcKNJK4kxscnKqEpKBo6cSgCPC6Ro8AlEeKcFEehemhor5unXCBc2XGxDI+7qPjF
# Emifz0DLQESlE/DmZAwlCEIysjaKJAL+L3J+HNdJRZboWR3p+nRka7LrZkPas7CM
# 1ekN3fYBIM6ZMWM9CBoYs4GbT8aTEAb8B4H6i9r5gkn3Ym6hU/oSlBiFLpKR6mhs
# RDKyZqHnGKSaZFHvMYIENzCCBDMCAQEwgYYwcjELMAkGA1UEBhMCVVMxFTATBgNV
# BAoTDERpZ2lDZXJ0IEluYzEZMBcGA1UECxMQd3d3LmRpZ2ljZXJ0LmNvbTExMC8G
# A1UEAxMoRGlnaUNlcnQgU0hBMiBBc3N1cmVkIElEIENvZGUgU2lnbmluZyBDQQIQ
# Bvr9sL154xnLx2xzI6KkMjAJBgUrDgMCGgUAoHgwGAYKKwYBBAGCNwIBDDEKMAig
# AoAAoQKAADAZBgkqhkiG9w0BCQMxDAYKKwYBBAGCNwIBBDAcBgorBgEEAYI3AgEL
# MQ4wDAYKKwYBBAGCNwIBFTAjBgkqhkiG9w0BCQQxFgQUAZwGZdBnbxTJwBzxJONs
# 0P9KkXIwDQYJKoZIhvcNAQEBBQAEggEAoxRxNkI366NYInwg/Lca4rsyGq7ThCjz
# 8t3bB16cnCaai5KA84duM5GNlUsjKLZws0Ea+VtsUeJhuIsCml6utnmn60P5wpug
# 1G6Fdcv7GFXWnUXYg3GyJ+B4yYHjyJ/DSSlLK+JxT09KOB2eqgwtdlNoyGGge4Av
# LaqFmqkGAR2bBTdduJ9BYNGIOXOSqbb+xqzopOnPHTbdGntxaN+Ar0Z+PeR4+VsG
# yqNnTDDhUeEPdwTdxxrPjYyF0/e3fdpkylNH8/BIJh8MCEvM3Cy9BAmgzH4Jw9E8
# eNCWE5t/TmeNhkDBF6OIQxPK3TjO3VUQKfnft6ZGQN6hyL7mCROwx6GCAgswggIH
# BgkqhkiG9w0BCQYxggH4MIIB9AIBATByMF4xCzAJBgNVBAYTAlVTMR0wGwYDVQQK
# ExRTeW1hbnRlYyBDb3Jwb3JhdGlvbjEwMC4GA1UEAxMnU3ltYW50ZWMgVGltZSBT
# dGFtcGluZyBTZXJ2aWNlcyBDQSAtIEcyAhAOz/Q4yP6/NW4E2GqYGxpQMAkGBSsO
# AwIaBQCgXTAYBgkqhkiG9w0BCQMxCwYJKoZIhvcNAQcBMBwGCSqGSIb3DQEJBTEP
# Fw0xNzA3MTMxODIzNDBaMCMGCSqGSIb3DQEJBDEWBBQMF9hX00FnDTGslqakDDJ2
# Lvn1fTANBgkqhkiG9w0BAQEFAASCAQCbdEjH5DJ16Mo/pPe3Ri06+koFJ8q0NZjH
# M/bBnmvu+8z/ibw5xk4DCsE2edWpoX+1LbT2UYNABRGh6RFtNxlo948aEtPlmbu/
# VclVjG1S0tHEARnc2BpG/G/mQ3lw7dmoECwl8tdMJ1lzPnl1RNk7Ntu/kZXRZogF
# HbXuuZvdtT1wxHeD+mXbaTkhiXwzpOu5eDpxOg5TKjApIADOVwS61v9cnaQvGA9L
# U9e1tnGV8T29yenEHGdC5uSXj1zV9Mp5pM9+q+gsQQ3PeEizOi+Z72YTstUtcRGE
# hHveYWN3AbwIF8i5j+ySo1j3+iNfsiVf1ePXbb/5BzJcnb+GMGll
# SIG # End signature block
